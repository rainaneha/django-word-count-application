from django.http import HttpResponse
from django.shortcuts import render
import operator
def homepage(request):
    return render(request, 'home.html')


def count(request):
    data = request.GET["fulltextarea"]
    wordlist = data.split()
    worddict = {}
    for item in wordlist:
        if item in worddict:
            worddict[item] += 1
        else:
            worddict[item] = 1

    sorted_list = sorted(worddict.items(), key= operator.itemgetter(1))

    return render(request, 'count.html' ,{'fulltext': data,  'worddictionary': sorted_list})

